chrome.runtime.onInstalled.addListener(onInstalled);
chrome.browserAction.onClicked.addListener(onBadgeClicked);
chrome.runtime.onMessage.addListener(handleMessages);
chrome.runtime.onMessage.addListener(getAboutPage);
chrome.tabs.onActivated.addListener(handleActivated);
chrome.tabs.onRemoved.addListener(removeTabs);
chrome.runtime.onMessage.addListener(openOptionsPage);

var settingUrl = browser.runtime.setUninstallURL(
  "https://help.reprintsdesk.com/hc/en-us/requests/new"
);
settingUrl.then(onSetURL, onError);

function onInstalled() {
  chrome.storage.sync.set(
    {
      floatingbutton: true,
      toolbar: true,
    },
    function () {
      console.log("Save init settings");
    }
  );
}

function onBadgeClicked(tab) {
  chrome.tabs.sendMessage(tab.id, { browserActionClicked: true, tab: tab });
}

function setBadge(count) {
  const badge = count ? count : "";
  const path = count ? "icons/logo.png" : "icons/logo_disabled.png";
  const message = count
    ? "New articles found in this page with Article Galaxy!"
    : "Article Galaxy Widget";
  const color = count ? "#F27E21" : "#FFFF";
  chrome.browserAction.setIcon({
    path: path,
  });
  chrome.browserAction.setBadgeBackgroundColor({ color: color });
  chrome.browserAction.setBadgeText({ text: `${badge}` });
  chrome.browserAction.setTitle({ title: `${message}` });
}

function handleMessages(msg) {
  const { count } = msg;
  chrome.tabs.query({ active: true, currentWindow: true }, function ([tab]) {
    if (tab && tab.id) {
      saveData({ count: count, id: tab.id });
      setBadge(count);
    }
  });
}

function getAboutPage(msg) {
  const { about } = msg;
  if (about) {
    chrome.tabs.create({ url: chrome.extension.getURL("assets/about.html") });
  }
}

function handleActivated({ tabId }) {
  chrome.storage.sync.get(["tabs"], function ({ tabs }) {
    if (tabs.length > 0) {
      tabs.find((tab) => {
        if (tab && tab.id === tabId) {
          setBadge(tab.count);
          console.log("con referencia", tabId);
        } else {
          console.log("sin reference");
        }
      });
    } else {
      setBadge(0);
    }
  });
}

function saveData(data) {
  chrome.storage.sync.get(["tabs"], async function ({ tabs }) {
    const { count, id } = data;
    if (count) {
      const item = await findItem(tabs, id);
      if (tabs.length > 0) {
        if (!item) {
          let result = [...tabs, data];
          console.log(result, "tabs");
          chrome.storage.sync.set({ tabs: result });
        }
      } else {
        let output = [data];
        console.log(output, "tabs");
        chrome.storage.sync.set({ tabs: output });
      }
    }
  });
}

function removeTabs(tabid) {
  chrome.storage.sync.get(["tabs"], function ({ tabs }) {
    let index = tabs.findIndex((item) => {
      return item.id === tabid;
    });
    if (index > -1) {
      tabs.splice(index, 1);
      console.log(tabs, "tabs");

      chrome.storage.sync.set({ tabs: tabs });
    }
  });
}

async function findItem(tabs, id) {
  const item = await tabs.find((item) => {
    return item.id === id;
  });
  return item;
}

function onSetURL() {
  chrome.storage.sync.remove(["floatingbutton", "toolbar"], function () {
    console.log("empty settings");
  });
}

function onError() {
  console.log("uninstalled");
}

function openOptionsPage({ options }) {
  if (options) {
    chrome.runtime.openOptionsPage();
  }
}

function onClean() {
  chrome.storage.sync.set({ tabs: [] });
}

onClean();
