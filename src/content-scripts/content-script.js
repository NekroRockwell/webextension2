const ignoredDomains = [
  "a-zlist.appspot.com",
  "reprintsdesk.com",
  "bibliogo.com",
  "article-galaxy-labs.appspot.com",
  "mozilla.org",
];
let count = null;
let types = [
  {
    id: "book-section",
    label: "Book Section",
    count: 1,
  },
  {
    id: "report",
    label: "Report",
    count: 3,
  },
  {
    id: "journal-article",
    label: "Journal Article",
    count: 15,
  },
  {
    id: "proceedings-article",
    label: "Proceedings Article",
    count: 1,
  },
];

onInit();
//When click on the icon on toolbar
chrome.runtime.onMessage.addListener(async function ({ browserActionClicked }) {
  if (!browserActionClicked) {
    return;
  }

  if (count) {
    toggleViews(true);
  }
});

// options was changed
chrome.storage.onChanged.addListener(async function (options) {
  const { toolbar, floatingbutton } = options;
  const settings = getSettings();

  if (toolbar) {
    const display = toolbar.newValue ? "flex" : "none";
    manageToolbar(display);
    manageFloatingActionButton("none");
  }

  if ((!toolbar && floatingbutton) || settings.floatingbutton) {
    const display2 = floatingbutton.newValue ? "block" : "none";
    manageFloatingActionButton(display2);
  }
});

function onInit() {
  let ignoreDomain = false;
  let i = 0;
  let bk = null;

  while (!ignoreDomain && i <= ignoredDomains.length) {
    ignoreDomain = location.hostname.includes(ignoredDomains[i]);
    i++;
  }
  if (ignoreDomain) {
    return;
  }

  chrome.storage.sync.get(["bk"], (result) => {
    if ("bk" in result) {
      bk = result.bk;
    } else {
      bk = ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
        (
          c ^
          (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
        ).toString(16)
      );
      chrome.storage.sync.set({ bk: bk }, () => {
        console.log("bk is set to " + bk);
      });
    }
    inyectAll(bk);
  });
}

function inyectAll(bk) {
  if (window.frames.length > 0 && "BODY" !== document.activeElement.tagName) {
    document.activeElement.blur();
  }
  if (document.getElementById("rd-startup-bk")) {
    return;
  }

  toggleViews();
  inyectStylesheet();
  createToolbar(count);
  createFAB(count);
  inyectScript(bk);
  setBadge(0);
}

function inyectStylesheet() {
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.type = "text/css";
  link.href = chrome.extension.getURL("assets/agw-style.css");
  document.head.appendChild(link);
}

function inyectScript(bk) {
  const a = document.createElement("div");
  a.innerHTML = `<div id="rd-startup-bk" class="${bk}" data-ssoid="" data-embedded="false" data-webextension="true" data-item-index="" style="display: none;"></div>`;
  document.body.appendChild(a);
  const src = document.createElement("script");
  src.setAttribute("id", "agw-order-multiple");
  document.body.appendChild(src).src =
    "https://redesign-dot-a-zlist.appspot.com/ordermultiple/ordermultiple.nocache.js";
}

function createToolbar(count) {
  var link = document.createElement("link");
  link.rel = "stylesheet";
  link.type = "text/css";
  link.href = chrome.extension.getURL("assets/agw-toolbar.css");
  const toolbar = document.createElement("div");
  toolbar.setAttribute("id", "agw-toolbar");
  toolbar.innerHTML = `
  <section  class="agw_toolbar">
    <div class="agw_toolbar_logo">
      <div></div>
    </div>
    <div class="agw_toolbar_mainInfo">
      <div class="agw_toolbar_counter">
        <i class="material-icons">arrow_forward_ios</i>
        <p>
            <strong id="count">${count} articles </strong> found 
        </p>
      </div>
      <span>
        <button type="button" id="agw-articles" class="agw_toolbar_fullAccess">
          ACCESS FULL-TEXT IN ARTICLE GALAXY
        </button>
        <div class="agw-tooltip-info"></div>
      </span>
    </div>
    <div class="agw_toolbar_actions">
      <div class="agw_toolbar_info">
        <span class="icon options">
          <i id="agw-options" class="material-icons">settings</i>
          <div class="agw-tooltip">Options</div>
        </span>
        <span class="icon cont">
          <i id="agw-info" class="material-icons-outlined">info</i>
          <div class="agw-tooltip">About Widget</div>
        </span>
        <span class="icon cont">
          <i id="help" class="material-icons">help</i>
          <div class="agw-tooltip">Help</div>
        </span>
      </div>
      <div id="agw-close" class="agw_toolbar_close">
        <span class="icon">
          <i class="material-icons">close</i>
        </span>
      </div>
    </div>
    </section>`;
  document.body.appendChild(toolbar);
  launchAGW();
  closeToolbar();
  addActionsEvents();
}

function createFAB(count) {
  const container = document.createElement("div");
  container.classList.add("agw-fab");
  container.classList.add("agw-circle");
  container.innerHTML = `
      <span class="agw-blue agw-circle"></span>
      <span class="agw-white agw-circle"></span>
      <span class="agw-orange-center agw-circle" id="agw-count">${count}</span>
      <span class="agw-orange-right agw-circle"></span>
      <span class="agw-orange-left agw-circle"></span>`;
  document.body.appendChild(container);
  document.querySelector(".agw-fab").addEventListener("click", function () {
    window.dispatchEvent(new Event("launchAgw"));
  });
}

function setBadge(count) {
  chrome.runtime.sendMessage({ count: count });
}

//Add close function to toolbar
function closeToolbar() {
  const close = document.querySelector("#agw-close");
  close.addEventListener("click", async function () {
    const { floatingbutton } = await getSettings();
    manageToolbar("none");
    const show = floatingbutton ? "block" : "none";
    manageFloatingActionButton(show);
  });
}

function addActionsEvents() {
  document.querySelector("#agw-options").addEventListener("click", function () {
    chrome.runtime.sendMessage({ options: true });
  });

  document.querySelector("#help").addEventListener("click", function () {
    window.open(
      "https://help.reprintsdesk.com/hc/en-us/requests/new",
      "_blank"
    );
  });

  document.querySelector("#agw-info").addEventListener("click", function () {
    chrome.runtime.sendMessage({ about: true });
  });
  document
    .querySelector("#agw-options")
    .addEventListener("mouseover", function (event) {
      event.target.nextElementSibling.style.display = "block";
    });
  document
    .querySelector("#agw-options")
    .addEventListener("mouseout", function (event) {
      event.target.nextElementSibling.style.display = "none";
    });
  document
    .querySelector("#agw-info")
    .addEventListener("mouseover", function (event) {
      event.target.nextElementSibling.style.display = "block";
    });
  document
    .querySelector("#agw-info")
    .addEventListener("mouseout", function (event) {
      event.target.nextElementSibling.style.display = "none";
    });
  document
    .querySelector("#help")
    .addEventListener("mouseover", function (event) {
      event.target.nextElementSibling.style.display = "block";
    });
  document
    .querySelector("#help")
    .addEventListener("mouseout", function (event) {
      event.target.nextElementSibling.style.display = "none";
    });
  document
    .querySelector("#agw-articles")
    .addEventListener("mouseover", function (event) {
      event.target.nextElementSibling.style.display = "block";
    });
  document
    .querySelector("#agw-articles")
    .addEventListener("mouseout", function (event) {
      event.target.nextElementSibling.style.display = "none";
    });
}
function addArticleInfo() {
  const ul = document.createElement("ul");
  for (let i = 0; i < types.length; i++) {
    const li = document.createElement("li");
    li.innerHTML = `${types[i].label} <strong>(${types[i].count})</strong>`;
    ul.appendChild(li);
  }
  return ul;
}

function generateTypes() {
  const ul = addArticleInfo();
  const info = document.querySelector(".agw-tooltip-info");
  info.appendChild(ul);
}

function manageToolbar(display) {
  const toolbar = document.querySelector("#agw-toolbar");
  if (toolbar && count) {
    toolbar.style.display = display;
  }
}

function manageFloatingActionButton(display) {
  const FAB = document.querySelector(".agw-fab");
  if (FAB && count) {
    const counter = document.querySelector("#agw-count");
    counter.textContent = count;
    FAB.style.display = display;
  }
}

async function toggleViews(clickOnIcon) {
  const { toolbar, floatingbutton } = await getSettings();

  if (!toolbar && !floatingbutton && clickOnIcon) {
    manageToolbar("flex");
    return;
  }
  const display = toolbar ? "flex" : "none";
  manageToolbar(display);

  const display2 = !toolbar && floatingbutton ? "block" : "none";
  manageFloatingActionButton(display2);
}

function launchAGW() {
  const launch = document.querySelector(".agw_toolbar_fullAccess");
  launch.addEventListener("click", function () {
    window.dispatchEvent(new Event("launchAgw"));
  });
}

async function getSettings() {
  const settings = new Promise(function (resolve) {
    chrome.storage.sync.get(["floatingbutton", "toolbar"], function (options) {
      resolve(options);
    });
  });
  return settings;
}

function showToolbar(citationCount) {
  if (citationCount) {
    console.log(types);
    count = citationCount;
    document.querySelector("#count").textContent = `${citationCount} articles `;
    generateTypes();
    toggleViews();
  } else {
    count = 0;
  }
}

window.addEventListener("showToolbar", function ({ detail }) {
  // Aqui viene la info para mas detalles
  let { citationCount } = detail;

  if (citationCount) {
    setBadge(citationCount);
    showToolbar(citationCount);
  }
});
